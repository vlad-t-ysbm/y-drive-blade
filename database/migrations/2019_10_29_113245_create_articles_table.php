<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('title_en');
            $table->string('title_pl');
            $table->string('title_ru');
            $table->string('slug')->unique();
            $table->longText('description_en');
            $table->longText('description_pl');
            $table->longText('description_ru');
            $table->text('short_description_en');
            $table->text('short_description_pl');
            $table->text('short_description_ru');
            $table->string('image')->nullable();
            $table->string('author');
            $table->string('reading_time');
            $table->string('topic_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
