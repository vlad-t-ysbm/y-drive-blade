<?php
return [
    //Легковое авто
//            'image' => '/img/car-fabia.png',
    [
        'title_ru' => 'Быстрое оформление',
        'title_en' => 'Quick clearance',
        'title_pl' => 'Szybka odprawa',
        'description_ru' => 'Оформим все документы и выдадим ключи всего за 30 минут.',
        'description_en' => 'We will issue all documents and issue keys in just 30 minutes.',
        'description_pl' => 'Wszystkie dokumenty i klucze wydamy w ciągu 30 minut.',
        'category_id' => 1
    ],
    [
        'title_ru' => 'Низкие цены',
        'title_en' => 'Low prices',
        'title_pl' => 'Niskie ceny',
        'description_ru' => 'Стоимость аренды авто начинается от 49 злотых в день.',
        'description_en' => 'The cost of renting a car starts from 49 PLN per day.',
        'description_pl' => 'Koszt wynajmu samochodu zaczyna się od 49 PLN za dzień.',
        'category_id' => 1
    ],
    [
        'title_ru' => 'Автомобили бензин/газ',
        'title_en' => 'Petrol / gas cars',
        'title_pl' => 'Samochody na gaz / gaz',
        'description_ru' => 'В наличии автомобили с сертифицированной газовой установкой.',
        'description_en' => 'Available cars with a certified gas installation.',
        'description_pl' => 'Dostępne samochody z certyfikowaną instalacją gazową.',
        'category_id' => 1
    ],
    [
        'title_ru' => 'Надежная страховка',
        'title_en' => 'Reliable insurance',
        'title_pl' => 'Niezawodne ubezpieczenie',
        'description_ru' => 'Страховые полисы ОС/АС на всех автомобилях уберегут вас от любых неожиданностей на дороге',
        'description_en' => 'OS / AC insurance policies on all vehicles will save you from any surprises on the road',
        'description_pl' => 'Polisy ubezpieczeniowe OS / AC na wszystkie pojazdy pozwolą Ci uniknąć niespodzianek na drodze',
        'category_id' => 1
    ],
    [
        'title_ru' => 'Экономичные авто',
        'title_en' => 'Economical cars',
        'title_pl' => 'Ekonomiczne samochody',
        'description_ru' => 'Средний расход наших авто 6л / 100км, что позволяет сэкономить до 350 злотых в месяц.',
        'description_en' => 'The average consumption of our cars is 6l / 100km, which saves up to 350 zlotys per month.',
        'description_pl' => 'Średnie zużycie naszych samochodów wynosi 6l / 100km, co pozwala zaoszczędzić do 350 złotych miesięcznie.',
        'category_id' => 1
    ],
    [
        'title_ru' => 'Сервис и качество',
        'title_en' => 'Service and quality',
        'title_pl' => 'Obsługa i jakość',
        'description_ru' => 'Наши автомобили всегда чистые и исправные. В случае неисправности мы предоставим замену в тот же день.',
        'description_en' => 'Our cars are always clean and in good condition. In the event of a malfunction, we will provide a replacement on the same day.',
        'description_pl' => 'Nasze samochody są zawsze czyste i sprawne. W przypadku awarii zapewniamy wymianę tego samego dnia.',
        'category_id' => 1
    ],

    //Грузовое авто
//            'image' => '/img/car2.png',
    [
        'title_ru' => 'Грузоподъемность и габариты (H2L3)',
        'title_en' => 'Dimensions and load capacity (H2L3)',
        'title_pl' => 'Wymiary i ładowność (H2L3)',
        'description_ru' => 'Длина: 3700 мм,<br>
                                     Ширина: 1870 мм,<br>
                                     Высота: 1900 мм,<br>
                                     Грузоподьемность: 1450 кг.',
        'description_en' => 'Length: 3700mm,<br>
                                     Width: 1870mm,<br>
                                     Height: 1900mm,<br>
                                     Payload: 1450 kg.',
        'description_pl' => 'Długość: 3700mm,<br>
                                     Szerokość: 1870mm,<br>
                                     Wysokość: 1900mm,<br>
                                     Ładowność: 1450 kg.',
        'category_id' => 2
    ],
    [
        'title_ru' => 'Быстрое оформление',
        'title_en' => 'Quick clearance',
        'title_pl' => 'Szybka odprawa',
        'description_ru' => 'Оформим все документы и выдадим ключи всего за 30 минут.',
        'description_en' => 'We will issue all documents and issue keys in just 30 minutes.',
        'description_pl' => 'Wszystkie dokumenty i klucze wydamy w ciągu 30 minut.',
        'category_id' => 2
    ],
    [
        'title_ru' => 'Экономичные авто',
        'title_en' => 'Economy cars',
        'title_pl' => 'Samochody ekonomiczne',
        'description_ru' => 'Средний расход наших авто 8.5л / 100км.',
        'description_en' => 'The average consumption of our cars is 8.5l / 100km.',
        'description_pl' => 'Średnie zużycie naszych samochodów wynosi 8.5l / 100km.',
        'category_id' => 2
    ],
    [
        'title_ru' => 'Надежная страховка',
        'title_en' => 'Reliable insurance',
        'title_pl' => 'Niezawodne ubezpieczenie',
        'description_ru' => 'Страховые полисы ОС/АС/NNW на всех автомобилях уберегут вас от любых неожиданностей на дороге',
        'description_en' => 'OS / AC / NNW insurance policies on all vehicles will save you from any surprises on the road',
        'description_pl' => 'Polisy ubezpieczeniowe OS / AC / NNW na wszystkie pojazdy pozwolą Ci uniknąć niespodzianek na drodze',
        'category_id' => 2
    ],
    [
        'title_ru' => 'Комфорт',
        'title_en' => 'Comfort',
        'title_pl' => 'Wygoda',
        'description_ru' => 'Все авто оборудованы кондиционерами и мультимедиа для удобства водителя.',
        'description_en' => 'All cars are equipped with air conditioning and multimedia for the convenience of the driver.',
        'description_pl' => 'Wszystkie samochody są wyposażone w klimatyzację i multimedia dla wygody kierowcy.',
        'category_id' => 2
    ],
    [
        'title_ru' => 'Вспомогательный инвентарь',
        'title_en' => 'Assistant Inventory',
        'title_pl' => 'Asystent zapasów',
        'description_ru' => 'Оборудование для крепежа и перевозок грузов.',
        'description_en' => 'Equipment for fasteners and cargo transportation.',
        'description_pl' => 'Sprzęt do zamocowań i transportu ładunków.',
        'category_id' => 2
    ],

    //Рено грузовое/пассажирское авто
//            'image' => '/img/car4.png',
    [
        'title_ru' => 'Автомобили бензин/газ',
        'title_en' => 'Petrol / gas cars',
        'title_pl' => 'Samochody na gaz / gaz',
        'description_ru' => 'Имеются автомобили с сертифицированной газовой установкой.',
        'description_en' => 'There are cars with a certified gas installation.',
        'description_pl' => 'Są samochody z certyfikowaną instalacją gazową.',
        'category_id' => 3
    ],
    [
        'title_ru' => 'Быстрое оформление',
        'title_en' => 'Quick clearance',
        'title_pl' => 'Szybka odprawa',
        'description_ru' => 'Оформим все документы и выдадим ключи всего за 30 минут.',
        'description_en' => 'We will issue all documents and issue keys in just 30 minutes.',
        'description_pl' => 'Wszystkie dokumenty i klucze wydamy w ciągu 30 minut.',
        'category_id' => 3
    ],
    [
        'title_ru' => 'Экономичные авто',
        'title_en' => 'Economy cars',
        'title_pl' => 'Samochody ekonomiczne',
        'description_ru' => 'Средний расход наших авто 9л / 100км.',
        'description_en' => 'The average consumption of our cars is 9l / 100km.',
        'description_pl' => 'Średnie zużycie naszych samochodów wynosi 9l / 100km.',
        'category_id' => 3
    ],
    [
        'title_ru' => 'Надежная страховка',
        'title_en' => 'Reliable insurance',
        'title_pl' => 'Niezawodne ubezpieczenie',
        'description_ru' => 'Страховые полисы ОС/АС/NNW на всех автомобилях уберегут вас от любых неожиданностей на дороге',
        'description_en' => 'OS / AC / NNW insurance policies on all vehicles will save you from any surprises on the road',
        'description_pl' => 'Polisy ubezpieczeniowe OS / AC / NNW na wszystkie pojazdy pozwolą Ci uniknąć niespodzianek na drodze',
        'category_id' => 3
    ],
    [
        'title_ru' => 'Высокая комплектация микроавтобусов',
        'title_en' => 'Highly equipped minibuses',
        'title_pl' => 'Dobrze wyposażone minibusy',
        'description_ru' => 'Круизконтроль,<br>
                                     Камера заднего вида/парктроник,<br>
                                     Сенсорная мультимедиа.',
        'description_en' => 'Cruise control,<br>
                                     Rear view camera/parking sensors,<br>
                                     Touch multimedia.',
        'description_pl' => 'Tempomat,<br>
                                     Kamera cofania/czujniki parkowania,<br>
                                     Dotknij multimediów.',
        'category_id' => 3
    ],
    [
        'title_ru' => 'Вместительность бусов',
        'title_en' => 'Minibus capacity',
        'title_pl' => 'Pojemność minibusa',
        'description_ru' => 'В наших микроавтобусах комфортно могут разместиться до 9 человек.',
        'description_en' => 'Our minibuses can comfortably accommodate up to 9 people.',
        'description_pl' => 'Nasze minibusy mogą wygodnie pomieścić do 9 osób.',
        'category_id' => 3
    ],
    //Велосипед
    [
        'title_ru' => '',
        'title_en' => '',
        'title_pl' => '',
        'description_ru' => 'Корпус Alpha Silver Aluminium, внутренняя прокладка кабеля, крепление дискового тормоза на нижней задней вилочной трубе, крепление в стойку и на ножку.',
        'description_en' => 'Alpha Silver Aluminum frame, internal cable routing, disc brake mount on lower rear fork tube, rack and foot mount.',
        'description_pl' => 'Rama Alpha Silver Aluminium wewnętrzne prowadzenie linek, mocowanie hamulca tarczowego na dolnej rurze widełek tylnych, mocowanie bagażnika i nóżki.',
        'category_id' => 4
    ],
    [
        'title_ru' => '',
        'title_en' => '',
        'title_pl' => '',
        'description_ru' => 'Гидравлический диск Tektro HD-275 (13,5 и 15,5: Tektro HD-276 с короткоходным рычагом).',
        'description_en' => 'Tektro HD-275 hydraulic disc (13.5 and 15.5: Tektro HD-276 with short stroke lever).',
        'description_pl' => 'Hydrauliczne tarczowe Tektro HD-275 (13,5 i 15,5: Tektro HD-276 z dźwignią o krótkim skoku).',
        'category_id' => 4
    ],
    [
        'title_ru' => '',
        'title_en' => '',
        'title_pl' => '',
        'description_ru' => '26" КОЛЕСА',
        'description_en' => '26" WHEELS.',
        'description_pl' => 'Rozmiar KÓŁ 26".',
        'category_id' => 4
    ],
    [
        'title_ru' => '',
        'title_en' => '',
        'title_pl' => '',
        'description_ru' => 'Мульти гонки - 7 передач.',
        'description_en' => 'Multi-race - 7 gears.',
        'description_pl' => 'Wielobigowy - 7 biegów.',
        'category_id' => 4
    ],
    [
        'title_ru' => '',
        'title_en' => '',
        'title_pl' => '',
        'description_ru' => 'Стойка для курьерских рюкзаков.',
        'description_en' => 'Rack for courier backpacks.',
        'description_pl' => 'Bagażnik pod plecaki kurierskie.',
        'category_id' => 4
    ],
    [
        'title_ru' => '',
        'title_en' => '',
        'title_pl' => '',
        'description_ru' => 'Комфорт: седло, поднятое положение, приблизительные тормоза.',
        'description_en' => 'Comfort: saddle, raised position, approximate brakes.',
        'description_pl' => 'Komfort: siodło, uniesiona pozycja, przybliżone hamulce.',
        'category_id' => 4
    ],
    //Велосипед Электро
    [
        'title_ru' => '',
        'title_en' => '',
        'title_pl' => '',
        'description_ru' => 'Аккумулятор Samsung 2200 мАч, 36В / 11Ач.',
        'description_en' => 'Samsung 2200 mAh battery, 36V / 11AH.',
        'description_pl' => 'Bateria  Samsung 2200 mAh, 36V / 11AH.',
        'category_id' => 5
    ],
    [
        'title_ru' => '',
        'title_en' => '',
        'title_pl' => '',
        'description_ru' => 'Дальность вождения на одном пополнении около 100 км.',
        'description_en' => 'Driving range on one top-up around 100 km.',
        'description_pl' => 'Zasięg jazdy na jednym doładowaniu około 100 km.',
        'category_id' => 5
    ],
    [
        'title_ru' => '',
        'title_en' => '',
        'title_pl' => '',
        'description_ru' => 'Полная зарядка за 5 часов.',
        'description_en' => 'Full charge in 5 hours.',
        'description_pl' => 'Pełne naładowanie w 5 godzin.',
        'category_id' => 5
    ],
    [
        'title_ru' => '',
        'title_en' => '',
        'title_pl' => '',
        'description_ru' => '26" КОЛЕСА',
        'description_en' => '26" WHEELS',
        'description_pl' => 'Rozmiar KÓŁ 26"',
        'category_id' => 5
    ],
    [
        'title_ru' => '',
        'title_en' => '',
        'title_pl' => '',
        'description_ru' => 'Мульти гонки - 7 передач.',
        'description_en' => 'Multi-race - 7 gears',
        'description_pl' => 'Wielobigowy - 7 biegów.',
        'category_id' => 5
    ],
    [
        'title_ru' => '',
        'title_en' => '',
        'title_pl' => '',
        'description_ru' => 'Стойка для курьерских рюкзаков.',
        'description_en' => 'Rack for courier backpacks.',
        'description_pl' => 'Bagażnik pod plecaki kurierskie.',
        'category_id' => 5
    ],
];
