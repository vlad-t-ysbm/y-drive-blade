<?php
return [
    [
        'id' => 1,
        'title_ru' => 'Арендовать легковое авто',
        'title_en' => 'Rent a car',
        'title_pl' => 'Wynajmij samochód',
        'image' => '/img/car.png'
    ],
    [
        'id' => 2,
        'title_ru' => 'Арендовать грузовой фургон',
        'title_en' => 'Rent a cargo van',
        'title_pl' => 'Wynajmij furgonetkę',
        'image' => '/img/car2.png'
    ],
    [
        'id' => 3,
        'title_ru' => 'Арендовать микроавтобус',
        'title_en' => 'Rent a minibus',
        'title_pl' => 'Wypożycz minibus',
        'image' => '/img/cargo.png'
    ],
    [
        'id' => 4,
        'title_ru' => 'Арендовать велосипед',
        'title_en' => 'Rent a bycicle',
        'title_pl' => 'Wypożycz rower',
        'image' => '/img/bike-1.png'
    ],
    [
        'id' => 5,
        'title_ru' => 'Арендовать электровелосипед',
        'title_en' => 'Rent an electric bycicle',
        'title_pl' => 'Wypożycz rower elektryczny',
        'image' => '/img/bike-2.png'
    ],
];
