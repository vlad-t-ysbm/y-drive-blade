<nav class="navbar navbar-expand-lg navbar-light bg-light" id="scrolled-nav">
    <div class="container">
        <div class="row w-100">
            <div class="col-lg-3">
                <a class="navbar-brand" href="/"></a>
                <button class="navbar-toggler mob-menu" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                        aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
            <div class="col-lg-7">
                <div class="collapse navbar-collapse nav-list" id="navbarNavDropdown">
{{--                    <ul class="navbar-nav nav-center">--}}
{{--                            <li class="nav-item mr-2">--}}
{{--                                <a class="nav-link color-black" href="{{route('blog.index', $lang)}}">{{trans('landing.nav_blog')}}<span class="sr-only">(current)</span></a>--}}
{{--                            </li>--}}
{{--                        <li class="nav-item mr-2">--}}
{{--                                <a class="nav-link color-black" href="#price">{{trans('landing.nav_price')}}<span class="sr-only">(current)</span></a>--}}
{{--                            </li>--}}
{{--                    </ul>--}}
                    <ul class="navbar-nav nav-center nav-list">
                        <li class="nav-item-language mr-2">
                            <a href="/pl{{substr(\Illuminate\Support\Facades\Request::path(), 2)}}" class="nav-link color-black language-border">PL</a>
                        </li>
                        <li class="nav-item-language mr-2">
                            <a href="/en{{substr(\Illuminate\Support\Facades\Request::path(), 2)}}" class="nav-link color-black language-border">EN</a>
                        </li>
                        <li class="nav-item-language mr-2">
                            <a href="/ru{{substr(\Illuminate\Support\Facades\Request::path(), 2)}}" class="nav-link color-black language-border">UA</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-2">
                <button type="button" class="btn green-btn not-mobile nav-center" id="contact-btn" data-toggle="modal"
                        data-target="#exampleModal">
                    {{ trans('landing.contact_btn') }}
                </button>
            </div>
        </div>
    </div>
</nav>

