<script src="{{asset('slick/slick.js')}}" type="text/javascript" charset="utf-8"></script>
<script async src={{asset("js/carousel.js")}} ></script>
<script>
    $(".regular").slick({
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: false,
        responsive: [
            {
                breakpoint: 1000,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 500,
                settings: {
                    arrows: false,
                    dots: true,
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
</script>
<script>
    $(function(){
        window.setTimeout(function(){
            $('#alert-landing').hide();
        },3000);
    });
</script>
<script>
    $(window).scroll(function(){
        if ($(this).scrollTop() > 25) {
            $('#scrolled-nav').addClass('scrolled');
        } else {
            $('#scrolled-nav').removeClass('scrolled');
        }
    });
</script>
<script>
    @if (count($errors) > 0)
    $(document).ready(function(){
        $('#exampleModal').modal({show: true});
    });
    @endif
</script>

<script>
    $(document).on('click', '.dich-button',function (e) {
        $('#title').val($(this).data('title'));
        $('#price').val($(this).data('price'));
        $('#category').val($(this).data('category'));
        $('#rentalModal').modal({show: true});
    });
    @if (count($errors) > 0)
    $(document).ready(function(){

    });

    @endif
</script>

<script>
    $(document).ready(function(){

        var _token = $('input[name="_token"]').val();



        // load_data(0, _token);

        function load_data(page, _token, search = '')
        {
            var topic = $('.selected-topic.active').data('topic-id');

            $.ajax({
                url:"{{ route('load_data', $lang) }}",
                method:"POST",
                data: {
                    page: page,
                    _token:_token,
                    topic: topic,
                    search: search
                },
                success:function(data)
                {
                    if (data.output) {
                        $('#post_data').append(data.output);
                    }
                    if (!data.pages || data.page == data.pages) {
                        $('#load_more_button').hide();
                    } else {
                        $('#load_more_button').data('page', data.page);
                        $('#load_more_button').data('pages', data.pages);
                        $('#load_more_button').show();
                    }

                }
            })
        }

        $(document).on('click', '#load_more_button', function(){
            var page = $(this).data('page');
            console.log($(this).data());
            load_data(page, _token, $('#search').val());
        });

        $(document).on('click', '.selected-topic', function () {
            $('.selected-topic').removeClass('active');
            $(this).addClass('active');
            $('#post_data').html('');
            $('#search').val('');
            load_data(0, _token);
        })
        $(document).on('input', '#search', function(){
            var search = $(this).val();
            console.log(search);
            $('#post_data').html('');
            load_data(0, _token, search);
        });
    });
</script>

<script>
    $(document).on('click', '.gallery-controls-previous, .gallery-controls-next', function () {
        let el = $('.gallery-item-selected');
        $('.dich-cat').removeClass('active');
        $('.dich-id-' + el.data('id')).addClass('active');
    })
</script>



