<section class="section-8">
    <div class="container">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-6 mobile-footer">
            <div class="section-8-title">
                <span>{{ trans("landing.contacts") }}</span>
            </div>
            <div class="section-8-subtitle">
                <span>{{ trans("landing.head_office") }}</span>
            </div>
            <div class="w-100 contact-item contact-email">
                <a href="mailto:{{$contacts['email']}}"><span>{{$contacts['email']}}</span></a>
            </div>
            <div class="w-100 contact-item">
                <span>{{$contacts['address']}}</span>
            </div>
            <div class="w-100 phone">
                <i class="phone-icon"></i>
                <a href="tel:{{$contacts['phone']}}"><span>{{$contacts['phone']}}</span></a>
                <div class="messenger">
                    <a href="viber://forward?text=780060917"><i class="icon viber"></i></a>
                    <a href="tg://resolve?domain=имя"><i class="icon telegram"></i></a>
                    <a href="whatsapp://send?phone=780060917"><i class="icon whatsapp"></i></a>
                    <a href="https://msng.link/fm/YdrivePolska/" target="_blank"><i class="icon fb-mes"></i></a>
                </div>
            </div>
            <button type="button" class="btn footer-btn" id="contact-btn" data-toggle="modal" data-target="#exampleModal">
                {{ trans('landing.contact_btn') }}
            </button>
        </div>
        <div class="col-md-6 not-mobile">
            <div class="img-block">
                <img class="footer-img" src="/img/footer.jpg">
            </div>
        </div>
        <div class="col-12 col-md-12 col-lg-8 margin-footer mobile-footer">
            <div class="rights">
            <span>
              2014-2019 All Rights RESERVED. YSBM Group sp. z o.o. KRS: 0000512023 NIP: 6762476939
            </span>
            </div>
            <div class="policies">
                <a href="{{route('legal-data', $lang)}}" class="gold-link">{{ trans("landing.legal_data") }}</a>
                <a href="{{route('coockie-policy', $lang)}}" class="gold-link">{{ trans("landing.coockie_policy") }}</a>
            </div>
        </div>
        <div class="col-12 col-md-12 col-lg-4 margin-footer">
            <div class="socials">
                <a href="{{$contacts['facebook']}}" class="facebook social-icon" target="_blank"></a>
            </div>
        </div>
    </div>
</div>
</section>

