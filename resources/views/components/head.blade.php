<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>Y-Drive</title>
<link rel="stylesheet" type="text/css" href="{{asset('slick/slick.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('slick/slick-theme.css')}}">
<!-- Fonts -->
<link rel="dns-prefetch" href="//fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
<!-- Styles -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link rel="shortcut icon" href="{{asset('img/favicon.png')}}" type="image/x-icon">
<!-- Scripts -->
{{--<script src="{{ asset('js/jquery.min.js') }}"></script>--}}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src="{{ asset('js/app.js') }}" async></script>
<link rel="stylesheet" href="{{asset('css/carousel.css') }}" />
{{--<script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>--}}

<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-KTPPXLQ');
</script>

