<div class="modal fade" id="rentalModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">{{ trans('landing.contact_us_modal') }}</h3>
            </div>
            <div class="modal-body">
                <form method="post" action="{{route('submitRent', app()->getLocale())}}">
                    @csrf
                    @method('post')
                    <div class="form-group">
                        <input type="text" class="form-control form-wrap-item border-0" id="name" name="name" placeholder="{{trans('landing.enter_name')}}" required>
                    </div>
                    <div class="form-group">
                        <input type="tel" class="form-control form-wrap-item border-0" id="phone" name="phone" placeholder="{{trans('landing.enter_phone')}}" required>
                        <input type="hidden" class="form-control form-wrap-item border-0" id="lang" name="lang" value="{{app()->getLocale()}}">
                    </div>
                    <div class="form-group data">
                    <input type="hidden" id="title" name="title">
                    <input type="hidden" id="price" name="price">
                    <input type="hidden" id="category" name="category">
                    </div>
                    @if($errors)
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $error }}</strong>
                            </div>
                        @endforeach
                    @endif
                    <div class="rules-recapcha-wrapper w-100">
                        <input class="rules-recapcha-checkbox" type="checkbox" required>
                        <span class="fw-500">{{ trans('landing.agree') }}</span>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn green-btn contact-btn">{{ trans('landing.submit') }}</button>
                    </div>
                    <div class="w-100 form-number">
                        <span class="fw-500">{{ trans('landing.call') }}</span>
                        <p class="font-weight-bold">
                            <a href="tel:{{$contacts['phone']}}" class="modal-phone">{{$contacts['phone']}}</a>
                        </p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
