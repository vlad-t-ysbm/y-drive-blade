@extends('layouts.app')

@section('content')
    <div class="container section-blog">
        <div class="row">
            <div class="col-12 text-center">
                <img src="/img/articles/{{$article->image}}" class="article-inner-image">
            </div>
            <div class="col-sm-12 col-md-12 col-lg-2 mt-4">
                <a href="{{route('blog.index', $lang)}}" class="back">
            <span><svg xmlns="http://www.w3.org/2000/svg" width="38" height="27" viewBox="0 0 38 27" style="width:38px;height:27px"
           class=""><path fill="currentColor"
                          d="M4.016 7.57L8.69 2.298A1.386 1.386 0 0 0 8.58.35a1.363 1.363 0 0 0-1.934.11L.657 7.216a2.615 2.615 0 0 0 0 3.467l5.99 6.755a1.363 1.363 0 0 0 1.933.111c.565-.507.615-1.38.11-1.948l-4.674-5.272h24.329c3.813 0 6.915 3.121 6.915 6.957 0 3.835-3.102 6.957-6.915 6.957h-4.467c-.756 0-1.37.617-1.37 1.379S23.122 27 23.878 27h4.467C33.669 27 38 22.642 38 17.285c0-5.358-4.331-9.715-9.655-9.715H4.015z"></path></svg> <span class="ml-1">
            All articles
          </span> </span>
                </a>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-10 mt-4">
                <div class="d-flex text-item-color">
                    <div class="mb-3 align-items-baseline d-flex">
                        <div class="mr-2">
                            <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25"
                                 class="text-black opacity-25">
                                <g fill="currentColor">
                                    <path
                                        d="M12.5 0C5.608 0 0 5.608 0 12.5S5.608 25 12.5 25 25 19.392 25 12.5 19.392 0 12.5 0zm0 22.34c-5.426 0-9.84-4.414-9.84-9.84s4.414-9.84 9.84-9.84 9.84 4.414 9.84 9.84-4.414 9.84-9.84 9.84z"></path>
                                    <path
                                        d="M18.936 11.908h-5.808V5.046c0-.578-.476-1.046-1.064-1.046C11.476 4 11 4.468 11 5.046v7.908c0 .578.476 1.046 1.064 1.046h6.872c.588 0 1.064-.468 1.064-1.046 0-.577-.476-1.046-1.064-1.046z"></path>
                                </g>
                            </svg>
                        </div>
                        <div class="flex items-center default-text">
                            Reading time: <span class="ml-1 inline-block font-bold font-weight-bold">{{$article->reading_time}}  minutes</span>
                        </div>
                    </div>
                    <div class="flex ml-3 mb-3 align-items-center d-flex">
                        <div class="mr-2">
                            <svg xmlns="http://www.w3.org/2000/svg" width="23" height="25" viewBox="0 0 23 25"
                                 class="text-black opacity-25">
                                <path fill="currentColor" fill-rule="evenodd"
                                      d="M2.846 22.1h17.307V9.594H2.846V22.1zM8.141 5.91c.784 0 1.423-.65 1.423-1.45h3.872c0 .8.639 1.45 1.423 1.45.785 0 1.424-.65 1.424-1.45h3.87v2.234H2.847V4.46h3.87c0 .8.64 1.45 1.425 1.45zm13.436-4.35h-5.294v-.11c0-.8-.639-1.45-1.424-1.45-.784 0-1.423.65-1.423 1.45v.11H9.564v-.11C9.564.65 8.925 0 8.141 0S6.717.65 6.717 1.45v.11H1.424C.64 1.56 0 2.21 0 3.01v20.539C0 24.349.639 25 1.424 25h20.153C22.36 25 23 24.349 23 23.549V3.009c0-.799-.639-1.45-1.423-1.45z"></path>
                            </svg>
                        </div>
                        <div class="default-text">
                            {{$article->updated_at->diffForHumans()}}
                        </div>
                    </div>
                </div>
                <div class="mb-3 blog-title">
                    <span> {{$article['title_'.$lang]}} </span>
                </div>
                <div class="mb-3 text-item-color description-font">
                    {!! $article['description_'.$lang] !!}
                </div>
            </div>

        </div>
    </div>
    </div>
@endsection
