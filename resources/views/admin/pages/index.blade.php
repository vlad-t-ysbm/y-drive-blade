@extends('layouts.admin-app')

@section('content')
    <a href="{{route('pages.create')}}" class="btn btn-info mb-1 w-100">Create page</a>
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Page title</th>
            <th scope="col">Page url</th>
            <th scope="col">Subpages</th>
            <th scope="col">Main page</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($pages as $key => $value)
        <tr class="admin-table">
            <th scope="row">{{$key+1}}</th>
            <td>{{$value->title}}</td>
            <td>{{$value->slug}}</td>
            <td>
                @if($value->parent_id == 0)
                @if($value->children()->count())
                <ul class="subpage-table">
                    @foreach($value->children() as $children)
                    <li><a href="/page/{{$children->slug}}">{{$children->title}}</a></li>
                    @endforeach
                </ul>
                @else
                    <p style="color: #ff6a00;">This is parent page without subpages</p>
                @endif
                @else
                    <p style="color: green">This page is a sub page of
                        @foreach($value->parents() as $parent)
                            <a href="/page/{{$parent->slug}}">{{$parent->title}}</a>
                        @endforeach
                    </p>
                @endif
            </td>
            <td class="text-center">
                @if($value->main_page)
                    <i class="fa fa-check check"></i>
                    @else
                @endif
            </td>
            <td class="d-flex">
                <a class="btn btn-warning action-button" href="{{route('pages.edit', $value->id)}}">Edit</a>
                @if($value->main_page === 1)

                @else
                    <form method="post" action="{{route('set-main-page', $value->id)}}">
                        @csrf
                        @method('post')
                        <button class="btn btn-success action-button ml-2" type="submit">Main page</button>
                    </form>
                @endif
                <form method="post" action="{{route('pages.destroy', $value->id)}}">
                    @csrf
                    @method('delete')
                    <button class="btn btn-danger action-button ml-2" type="submit">Delete</button>
                </form>

            </td>
{{--            <td></td>--}}
        </tr>
        @endforeach
        </tbody>
    </table>
@endsection
