@if ($message = Session::get('success'))
    <div id="alert-admin" style="text-align: center" class="alert alert-success alert-dismissible fade show" role="alert">
{{--        <button type="button" class="close" data-dismiss="alert">×</button>--}}
        <strong>{{ $message }}</strong>
    </div>
@endif

@if ($message = Session::get('error'))
    <div id="alert-admin" style="text-align: center" class="alert alert-danger alert-dismissible fade show" role="alert">
{{--        <button type="button" style="text-align: center;" class="close" data-dismiss="alert">×</button>--}}
        <strong>{{ $message }}</strong>
    </div>

@endif

@if ($message = Session::get('warning'))
    <div id="alert-admin" class="alert alert-warning alert-dismissible fade show" role="alert">
{{--        <button type="button" class="close" data-dismiss="alert">×</button>--}}
        <strong>{{ $message }}</strong>
    </div>
@endif

@if ($message = Session::get('info'))
    <div id="alert-admin" class="alert alert-info alert-dismissible fade show" role="alert">
{{--        <button type="button" class="close" data-dismiss="alert">×</button>--}}
        <strong>{{ $message }}</strong>
    </div>
@endif

@if ($errors->any())
    @foreach ($errors->all() as $error)
{{--        <div id="my-alert" class="alert alert-danger alert-dismissible fade show" role="alert">--}}
        <div class="alert alert-danger alert-dismissible fade show" style="text-align: center" id="alert-admin">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $error }}</strong>
        </div>
    @endforeach
@endif
