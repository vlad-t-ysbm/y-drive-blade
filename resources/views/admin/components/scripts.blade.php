<script type="text/javascript">
    tinymce.init({
        selector : "textarea",
        height: 500,
        plugins : ["advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", "textcolor", "insertdatetime media table contextmenu paste jbimages"],
        toolbar : "insertfile undo redo | styleselect | bold italic strikethrough  forecolor backcolor | fontsizeselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | jbimages",
        relative_urls: false
    });
</script>
{{--"insertdatetime media table contextmenu paste jbimages"--}}
<!--jbimage-->
<script>
    $(function(){
        window.setTimeout(function(){
            $('#alert-admin').alert('close');
        },3000);
    });
</script>

