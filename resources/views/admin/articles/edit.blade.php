@extends('layouts.admin-app')

@section('content')
    <form method="post" action="{{route('articles.update', $article->id)}}" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="form-group form-border">
            <div class="d-inline-flex">
            <i data-v-5ba3d74b="" class="icon poland"></i>
            <label class="admin-label" for="exampleFormControlInput1">TITLE PL</label>
            </div>
            <input type="text" class="form-control" id="exampleFormControlInput1" value="{{$article->title_pl}}" name="title_pl">
        </div>
        <div class="form-group form-border">
            <div class="d-inline-flex">
            <i data-v-5ba3d74b="" class="icon uk"></i>
            <label class="admin-label" for="exampleFormControlInput1">TITLE EN</label>
            </div>
            <input type="text" class="form-control" id="exampleFormControlInput1" value="{{$article->title_en}}" name="title_en">
        </div>
        <div class="form-group form-border">
            <div class="d-inline-flex">
            <i data-v-5ba3d74b="" class="icon russia"></i>
            <label class="admin-label" for="exampleFormControlInput1">TITLE RU</label>
            </div>
            <input type="text" class="form-control" id="exampleFormControlInput1" value="{{$article->title_ru}}" name="title_ru">
        </div>
        <div class="form-group form-border">
            <label for="exampleFormControlInput1">Slug</label>
            <input type="text" class="form-control" id="exampleFormControlInput1" value="{{$article->slug}}" name="slug">
        </div>
        <div class="form-group form-border">
            <label for="exampleFormControlFile1">Image</label>
            <div>
                <label>Current image</label>
            <img class="d-block mb-2" src="/img/articles/{{$article->image}}" width="200" height="200">
            </div>
            <input type="file" class="form-control-file" id="exampleFormControlFile1" value="" name="image">
        </div>
        <div class="form-group form-border">
            <div class="d-inline-flex">
            <i data-v-5ba3d74b="" class="icon poland"></i>
            <label class="admin-label" for="exampleFormControlTextarea1">DESCRIPTION PL</label>
            </div>
            <textarea class="form-control" id="exampleFormControlTextarea1" name="description_pl" rows="5">{{$article->description_pl}}</textarea>
        </div>
        <div class="form-group form-border">
            <div class="d-inline-flex">
            <i data-v-5ba3d74b="" class="icon uk"></i>
            <label class="admin-label" for="exampleFormControlTextarea1">DESCRIPTION EN</label>
            </div>
            <textarea class="form-control" id="exampleFormControlTextarea1" name="description_en" rows="5">{{$article->description_en}}</textarea>
        </div>
        <div class="form-group form-border">
            <div class="d-inline-flex">
            <i data-v-5ba3d74b="" class="icon russia"></i>
            <label class="admin-label" for="exampleFormControlTextarea1">DESCRIPTION RU</label>
            </div>
            <textarea class="form-control" id="exampleFormControlTextarea1" name="description_ru" rows="5">{{$article->description_ru}}</textarea>
        </div>
        <div class="form-group form-border">
            <div class="d-inline-flex">
            <i data-v-5ba3d74b="" class="icon poland"></i>
            <label class="admin-label" for="exampleFormControlTextarea2">SHORT DESCRIPTION PL</label>
            </div>
            <textarea class="form-control" id="exampleFormControlTextarea2" name="short_description_pl" rows="5">{!! $article->short_description_pl !!}</textarea>
        </div>
        <div class="form-group form-border">
            <div class="d-inline-flex">
            <i data-v-5ba3d74b="" class="icon uk"></i>
            <label class="admin-label" for="exampleFormControlTextarea2">SHORT DESCRIPTION EN</label>
            </div>
            <textarea class="form-control" id="exampleFormControlTextarea2" name="short_description_en" rows="5">{!! $article->short_description_en !!}</textarea>
        </div>
        <div class="form-group form-border">
            <div class="d-inline-flex">
            <i data-v-5ba3d74b="" class="icon russia"></i>
            <label class="admin-label" for="exampleFormControlTextarea2">SHORT DESCRIPTION RU</label>
            </div>
            <textarea class="form-control" id="exampleFormControlTextarea2" name="short_description_ru" rows="5">{!! $article->short_description_ru !!}</textarea>
        </div>
        <div class="form-group form-border">
            <label for="exampleFormControlInput2">Author</label>
            <input type="text" class="form-control" id="exampleFormControlInput2" name="author" value="{{$article->author}}">
        </div>
        <div class="form-group form-border">
            <label for="exampleFormControlInput3">Reading time</label>
            <input type="text" class="form-control" id="exampleFormControlInput3" name="reading_time" value="{{$article->reading_time}}">
        </div>
        <div class="form-group form-border">
            <label for="exampleFormControlSelect1">Topic</label>
            {{$article->topic_id}}
            <select class="form-control" id="exampleFormControlSelect1" name="topic_id">
                <option value="0">None</option>
                @foreach($topics as $topic)
                    @if($topic->id == $article->topic_id)
                        <option value="{{$topic->id}}" selected>{{$topic->title_pl}}</option>
                    @else
                    <option value="{{$topic->id}}">{{$topic->title_pl}}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-success">SAVE</button>
    </form>
@endsection


