@extends('layouts.admin-app')

@section('content')
    <form method="post" action="{{route('articles.store')}}" enctype="multipart/form-data">
        @csrf
        <div class="form-group form-border">
            <label for="exampleFormControlInput1">TITLE PL</label>
            <div class="d-flex">
                <i data-v-5ba3d74b="" class="icon poland"></i>
            <input type="text" class="form-control" id="exampleFormControlInput1" name="title_pl">
            </div>
            </div>
        <div class="form-group form-border">
            <label for="exampleFormControlInput1">TITLE EN</label>
            <div class="d-flex">
                <i data-v-5ba3d74b="" class="icon uk"></i>
            <input type="text" class="form-control" id="exampleFormControlInput1" name="title_en">
            </div>
            </div>
        <div class="form-group form-border">
            <label for="exampleFormControlInput1">TITLE RU</label>
            <div class="d-flex">
                <i data-v-5ba3d74b="" class="icon russia"></i>
            <input type="text" class="form-control" id="exampleFormControlInput1" name="title_ru">
            </div>
            </div>
        <div class="form-group form-border">
            <label for="exampleFormControlInput1">SLUG</label>
            <input type="text" class="form-control" id="exampleFormControlInput1" name="slug">
        </div>
        <div class="form-group form-border">
            <label for="exampleFormControlFile1">IMAGE</label>
            <input type="file" class="form-control-file" id="exampleFormControlFile1" name="image">
        </div>
        <div class="form-group form-border">
            <div class="d-inline-flex">
            <i data-v-5ba3d74b="" class="icon poland"></i>
            <label class="admin-label" for="exampleFormControlTextarea1">DESCRIPTION PL</label>
            </div>
            <textarea class="form-control post" id="exampleFormControlTextarea1" name="description_pl" rows="5"></textarea>

        </div>
        <div class="form-group form-border">
            <div class="d-inline-flex">
                <i data-v-5ba3d74b="" class="icon uk"></i>
                <label class="admin-label" for="exampleFormControlTextarea1">DESCRIPTION EN</label>
            </div>
            <textarea class="form-control post" id="exampleFormControlTextarea1" name="description_en" rows="5"></textarea>
        </div>
        <div class="form-group form-border">
            <div class="d-inline-flex">
                <i data-v-5ba3d74b="" class="icon russia"></i>
                <label class="admin-label" for="exampleFormControlTextarea1">DESCRIPTION RU</label>
            </div>
            <textarea class="form-control post" id="exampleFormControlTextarea1" name="description_ru" rows="5"></textarea>
        </div>
        <div class="form-group form-border">
            <div class="d-inline-flex">
                <i data-v-5ba3d74b="" class="icon poland"></i>
                <label class="admin-label" for="exampleFormControlTextarea2">SHORT DESCRIPTION PL</label>
            </div>
            <textarea class="form-control post" id="exampleFormControlTextarea2" name="short_description_pl" rows="5"></textarea>
        </div>
        <div class="form-group form-border">
            <div class="d-inline-flex">
                <i data-v-5ba3d74b="" class="icon uk"></i>
                <label class="admin-label" for="exampleFormControlTextarea2">SHORT DESCRIPTION EN</label>
            </div>
            <textarea class="form-control post" id="exampleFormControlTextarea2" name="short_description_en" rows="5"></textarea>
        </div>
        <div class="form-group form-border">
            <div class="d-inline-flex">
                <i data-v-5ba3d74b="" class="icon russia"></i>
                <label class="admin-label" for="exampleFormControlTextarea2">SHORT DESCRIPTION RU</label>
            </div>
            <textarea class="form-control post" id="exampleFormControlTextarea2" name="short_description_ru" rows="5"></textarea>
        </div>
        <div class="form-group form-border">
            <label for="exampleFormControlInput2">AUTHOR</label>
            <input type="text" class="form-control" id="exampleFormControlInput2" name="author">
        </div>
        <div class="form-group form-border">
            <label for="exampleFormControlInput3">READING TIME</label>
            <input type="text" class="form-control" id="exampleFormControlInput3" name="reading_time">
        </div>
        <div class="form-group form-border">
            <label for="exampleFormControlSelect1">TOPIC</label>
            <select class="form-control" id="exampleFormControlSelect1" name="topic_id">
                <option value="0">None</option>
                @foreach($topics as $topic)
                <option value="{{$topic->id}}">{{$topic->title_pl}}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-success">SAVE</button>
    </form>
@endsection
