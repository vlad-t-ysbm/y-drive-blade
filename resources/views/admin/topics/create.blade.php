@extends('layouts.admin-app')

@section('content')
    <form method="post" action="{{route('topics.store')}}" enctype="multipart/form-data">
        @csrf
        <div class="form-group form-border">
            <label for="exampleFormControlInput1">TITLE PL</label>
            <div class="d-flex">
            <i data-v-5ba3d74b="" class="icon poland"></i>
            <input type="text" class="form-control" id="exampleFormControlInput1" name="title_pl">
            </div>
        </div>
        <div class="form-group form-border">
            <label for="exampleFormControlInput1">TITLE EN</label>
            <div class="d-flex">
            <i data-v-5ba3d74b="" class="icon uk"></i>
            <input type="text" class="form-control" id="exampleFormControlInput1" name="title_en">
            </div>
        </div>
        <div class="form-group form-border">
            <label for="exampleFormControlInput1">TITLE RU</label>
            <div class="d-flex">
            <i data-v-5ba3d74b="" class="icon russia"></i>
            <input type="text" class="form-control" id="exampleFormControlInput1" name="title_ru">
            </div>
        </div>
        <button type="submit" class="btn btn-success">Save</button>
    </form>
@endsection
