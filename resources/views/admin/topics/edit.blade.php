@extends('layouts.admin-app')

@section('content')
    <form method="post" action="{{route('topics.update', $topic->id)}}">
        @csrf
        @method('put')
        <div class="form-group form-border">
            <label for="exampleFormControlInput1">Title PL</label>
            <div class="d-flex">
                <i data-v-5ba3d74b="" class="icon poland"></i>
                <input type="text" class="form-control" id="exampleFormControlInput1" name="title_pl" value="{{$topic->title_pl}}">
            </div>
            </div>
        <div class="form-group form-border">
            <label for="exampleFormControlInput1">Title EN</label>
            <div class="d-flex">
                <i data-v-5ba3d74b="" class="icon uk"></i>
                <input type="text" class="form-control" id="exampleFormControlInput1" name="title_en" value="{{$topic->title_en}}">
            </div>
            </div>
        <div class="form-group form-border">
            <label for="exampleFormControlInput1">Title RU</label>
            <div class="d-flex">
                <i data-v-5ba3d74b="" class="icon russia"></i>
                <input type="text" class="form-control" id="exampleFormControlInput1" name="title_ru" value="{{$topic->title_ru}}">
            </div>
            </div>
        <button type="submit" class="btn btn-success">Save</button>
    </form>
@endsection
