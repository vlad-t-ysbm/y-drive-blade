@extends('layouts.app')

@section('content')
    <section class="section-item">
        <div class="carousel-caption carousel-caption-item">
            <div>
                <span class="first-title">{{trans('landing.mainTitle1')}}</span>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 text-center">
                <button type="button" class="btn footer-btn" id="contact-btn" data-toggle="modal"
                        data-target="#exampleModal">{{trans('landing.contact_btn')}}</button>
            </div>
        </div>
    </section>
    <section class="category-section">
        <div class="container-fluid">
            <div class="row text-center mb-5">
                <div class="col-12 my-5">
                    <h2>{{trans('landing.taxi')}}</h2>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 px-0">
                    <div class="taxi">
                        <div class="post-image">
                            <div class="category-header">
                                <div><a href="https://mbpartners.pl/category/bezkategorii/"
                                        title="View all posts in Brak kategorii">{{trans('landing.taxi_subtitle_1')}}</a>
                                </div>
                                <h3 class="post-title"><a href="https://mbpartners.pl/2020/01/02/2020/" target="_self"
                                                          title=".2020">{{trans('landing.taxi_title_1')}}</a></h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 px-0">
                    <div class="deliveryman">
                        <div class="post-image">
                            <div class="category-header">
                                <div><a href="https://mbpartners.pl/category/bezkategorii/"
                                        title="View all posts in Brak kategorii">{{trans('landing.taxi_subtitle_2')}}</a>
                                </div>
                                <h3 class="post-title"><a href="https://mbpartners.pl/2020/01/02/2020/" target="_self"
                                                          title=".2020">{{trans('landing.taxi_title_2')}}</a></h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 px-0">
                    <div class="courier">
                        <div class="post-image">
                            <div class="category-header">
                                <div><a href="https://mbpartners.pl/category/bezkategorii/"
                                        title="View all posts in Brak kategorii">{{trans('landing.taxi_subtitle_3')}}</a>
                                </div>
                                <h3 class="post-title"><a href="https://mbpartners.pl/2020/01/02/2020/" target="_self"
                                                          title=".2020">{{trans('landing.taxi_title_3')}}</a></h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="text-center section-1">
        <div class="contaner">
        <div class="row m-0">
            <div class="col-xs-12 col-md-12 col-lg-4 mt-lg-4">
                <span class="experience">{{trans('landing.fleet')}}</span>
            </div>
            <div class="col-xs-12 col-md-12 col-lg-4 mt-lg-4">
                <span class="experience">{{ trans('landing.experience') }}</span>
            </div>
            <div class="col-xs-12 col-md-12 col-lg-4 mt-lg-4">
                <span class="experience">{{ trans('landing.condition') }}</span>
            </div>
            <div class="col-md-12 lh">

                <h1 class="main-title">
                    {{ trans('landing.main_title') }}
                </h1>
                <span class="choose-auto">{{ trans('landing.choose_category') }}</span>
            </div>
{{--            @foreach($categories as $category)--}}
{{--                <div class="col-md-4 category-block change-cat" data-id="{{$category['id']}}">--}}
{{--                    <a href="#section-3" class="category-link">--}}
{{--                        <div class="category-hover">--}}
{{--                            <img class="category-image" src="{{$category['image']}}">--}}
{{--                            <p class="category-title">--}}
{{--                                {{$category['title_'.$lang]}}--}}
{{--                            </p>--}}
{{--                        </div>--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--            @endforeach--}}

            <div class="gallery">
                <div class="gallery-container">
                    @foreach($categories as $category)
                        <div class="gallery-item" data-id="{{$category['id']}}">
                        <img src="{{$category['image']}}">

                        <p class="category-title">{{$category['title_'.$lang]}}</p>
                    </div>
                    @endforeach
                </div>
                <div class="gallery-controls"></div>
            </div>
            <button type="button" class="btn mobile-contact for-mobile nav-center" id="contact-btn" data-toggle="modal"
                    data-target="#exampleModal">
                {{ trans('landing.contact_btn') }}
            </button>
        </div>
        </div>
    </section>
    <section class="section-2">
        <div class="container text-uppercase">
            <span class="section-2-item-title">{{ trans('landing.auto_for') }}</span>
            <span class="section-2-item">{{ trans('landing.uber') }}</span>
            <span class="section-2-item">{{ trans('landing.bold') }}</span>
            <span class="section-2-item">{{ trans('landing.temporary') }}</span>
            <span class="section-2-item">{{ trans('landing.transfers') }}</span>
            <span class="section-2-item">{{ trans('landing.cargo_transportation') }}</span>
        </div>
        <div class="container text-uppercase">
            <span class="section-2-item-title">{{ trans('landing.bike_for') }}</span>
            <span class="section-2-item">{{ trans('landing.uber_eats') }}</span>
            <span class="section-2-item">{{ trans('landing.glovo') }}</span>
            <span class="section-2-item">{{ trans('landing.wolt') }}</span>
        </div>
    </section>
    <section ref="scroll" class="section-3 text-center" id="section-3">
        <div class="container">
            <span class="section-title">{{ trans('landing.conditions') }}</span>
            @foreach($advantages as $i => $adv)
                <div class="row mt-5 dich-cat dich-id-{{$adv[0]['category_id']}} @if ($i == 0) active @endif">
                    <div class="col-6 col-sm-6 col-md-6 col-lg-3 advantages-item-order">
                        <ul class="text-center advantages-first position-relative">
                            @foreach($adv as $key => $advantage)
                                @if($key % 2 !== 0)
                                    <li class="advantages-border mb-3">
                                        <div class="w-100">
              <span class="advantages-title">
                {{$advantage['title_'.$lang]}}
              </span>
                                        </div>
                                        <div class="w-100">
                                            <p class="advantages-description">
                                                {!! $advantage['description_'.$lang]!!}
                                            </p>
                                        </div>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-xs-12 col-md-12 col-lg-6 advantages-image-order">
                        <img src="{{$categories[$advantage['category_id']]['image']}}" class="mw-100 section-3-image">
                    </div>
                    <div class="col-6 col-sm-6 col-md-6  col-lg-3">
                        <ul class="text-center advantages-second position-relative">
                            @foreach($adv as $key => $advantage)
                                @if($key % 2 === 0)
                                    <li class="advantages-border mb-3">
                                        <div class="w-100">
              <span class="advantages-title">
                {{$advantage['title_'.$lang]}}
              </span>
                                        </div>
                                        <div class="w-100">
                                            <p class="advantages-description">
                                                {!! $advantage['description_'.$lang]!!}
                                            </p>
                                        </div>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
    <section ref="scroll" class="section-5 text-center" id="price">
        <div class="section-5-item">
            <div class="row">
                <div class="md-12 w-100">
                    <span class="section-title">{{ trans('landing.rent') }}</span>
                    <p>{{ trans('landing.rent_subtitle') }}</p>
                </div>
                <div class="container">
                    <div class="condition-category flex-wrap justify-content-center">
                        @foreach($categories as $category)
                            <div class="mb-3 col-xs-12 col-md-12 col-lg-4">
                                <button class="category-button change-cat" data-id="{{$category['id']}}">
                                    {{$category['title_'.$lang]}}
                                </button>
                            </div>
                        @endforeach
                    </div>
                </div>
                @foreach($conditions as $i => $con)
                    <div class="rent-block dich-cat dich-id-{{$con[0]['category_id']}} @if ($i == 0) active @endif">
                        @foreach($con as $key => $condition)
                            <div class="card mb-3 col-xs-12 col-md-12 rent-card"
                                 style="max-width: 18rem;"
                            >
                                @if($condition['popular'] == 1)
                                    <span class="popular">{{ trans('landing.popular') }}</span>
                                @endif
                                <div class="card-header rent-title">
                                    {{$condition['title_'.$lang]}}
                                </div>
                                <div class="card-body">
                                    <h5 class="card-title">
                                        {{$condition['price']}} {{ trans('landing.currency') }}
                                        @if($condition['category_id'] == 1)
                                            {{ trans('landing.per_day') }}
                                        @endif
                                    </h5>
                                    <button type="button" class="btn gold-btn dich-button" id="contact-btn"
                                            data-target="#rentalModal" data-title="{{$condition['title_pl']}}"
                                            data-price="{{$condition['price']}}"
                                            data-category="{{$condition['category_id']}}">
                                        {{ trans("landing.to_order") }}
                                    </button>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endforeach
                <div class="rent-contact col-12">
                    <span>{{ trans('landing.contact_us') }}<span><a href="tel:{{$contacts['phone']}}"
                                                                    class="rent-phone">{{$contacts['phone']}}</a></span></span>
                </div>
            </div>
        </div>
    </section>
{{--    <section ref="scroll" class="section-4 text-center">--}}
{{--        <div class="container pb-5">--}}
{{--            <span class="section-title">{{trans('landing.reviews')}}</span>--}}
{{--            @foreach($reviews as $i => $rev)--}}
{{--            <section class="regular slider mt-4 dich-cat dich-id-{{$rev[0]['category_id']}} @if($i === 0) active @endif">--}}
{{--                @foreach($rev as $review)--}}
{{--                <div>--}}
{{--                    <iframe width="300" height="200"--}}
{{--                            src="{{$review['link']}}"--}}
{{--                            frameborder="0" allow="autoplay; encrypted-media" allowfullscreen--}}
{{--                    ></iframe>--}}
{{--                    <div class="review-author">--}}
{{--                        <span>{{ $review['name_'.$lang] }}</span>--}}
{{--                        <span>{{ $review['service_'.$lang] }}</span>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                @endforeach--}}
{{--            </section>--}}
{{--            @endforeach--}}
{{--        </div>--}}
{{--    </section>--}}
@endsection
