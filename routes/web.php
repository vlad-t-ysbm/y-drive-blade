<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Site
Route::redirect('/', '/pl');
Route::get('/{lang}', 'Blog\BlogController@index');
//Route::get('/{lang}/page/{slug}', 'Blog\BlogController@show')->name('page');
Route::group(['prefix' => '{lang}'], function (){

});
//Route::resource('{lang}/blog','Articles\ArticlesController');
//Route::get('/blog/topic/{slug}', 'Articles\ArticlesController@showBySlug')->name('show-by-slug');
//Route::post('/blog/topic/{slug}', 'Articles\ArticlesController@loadBySlug')->name('show-by-slug-topic');
Route::get('{lang}/coockie-policy', 'Blog\PoliciesController@coockiePolicy')->name('coockie-policy');
Route::get('{lang}/legal-data', 'Blog\PoliciesController@legalData')->name('legal-data');
Route::post('{lang}/blog/load_data', 'Articles\ArticlesController@load_data')->name('load_data');
//});
//Route::post('/blog/by-topic', 'Articles\ArticlesController@getByTopic')->name('by-topic');
Auth::routes();

// Auth
Route::get('/register', 'Admin\AdminController@redirect');
Route::get('/password/reset', 'Admin\AdminController@redirect');
Route::get('/{lang}/admin', 'Admin\AdminController@admin');
Route::post('{lang}/submit','Blog\SendMailController@submit')->name('send-mail');
Route::post('{lang}/submit-rent','Blog\SendMailController@submitRent')->name('submitRent');
route::get('/changeLanguage/redirect/{url}/{lang}', 'Blog\BlogController@changeUrl')->name('redirect');
// Admin
//Route::group(['middleware'=>'auth', 'prefix'=>'admin'],function () {
////    Route::resource('/pages', 'Admin\PagesController');
//    Route::resource('/articles', 'Admin\ArticlesController');
//    Route::resource('/topics', 'Admin\TopicsController');
//    Route::post('/pages/{page}/set-main-page', 'Admin\PagesController@setMainPage')->name('set-main-page');
//});



