<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = [
        'title_en', 'title_pl', 'title_ru', 'description_en', 'description_pl', 'description_ru', 'short_description_en', 'short_description_pl' , 'short_description_ru', 'author', 'reading_time', 'slug', 'topic_id'
    ];

    public function topics(){
        return $this->belongsTo('App\Topic', 'topic_id')->get();
    }
}
