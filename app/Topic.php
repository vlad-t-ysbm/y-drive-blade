<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    protected $fillable = [
      'title_en', 'title_pl', 'title_ru'
    ];

    public function articles(){
        return $this->hasMany('App\Article', 'topic_id')->get();
    }
}
