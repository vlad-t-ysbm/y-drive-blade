<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Topic;
use Illuminate\Http\Request;

class TopicsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $topics = Topic::all();
        return view('admin.topics.index')->with('topics', $topics);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.topics.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Topic $topic)
    {
        try {
            $topic->create($request->all());
            return redirect()->route('topics.index')->with('success', 'Topic was successful created');
        }catch (\Exception $e){
            return redirect()->back()->with('error', (string)$e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $topic = Topic::find($id);
        return view('admin.topics.edit')->with('topic', $topic);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
       $topic = Topic::where('id', $id)->first();
       $topic->update($request->all());

       return redirect()->route('topics.index')->with('success', 'Topic successful updated');
        }catch (\Exception $e){
            return redirect()->back()->with('error', (string)$e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $topic = Topic::find($id);
            $topic->delete();

            return back()->with('success', 'Topic was successful deleted');
        }catch (\Exception $e){
            return redirect()->back()->with('error', (string)$e->getMessage());
        }
    }
}
