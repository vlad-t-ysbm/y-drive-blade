<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\Http\Controllers\Controller;
use App\Topic;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::orderBy('created_at', 'desc')->get();

        return view('admin.articles.index')->with('articles', $articles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $topics = Topic::all();
        return view('admin.articles.create')->with('topics', $topics);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Article $article)
    {
        $name = Carbon::now()->toTimeString(). '_' . Carbon::now()->toDateString(). '_' .$request->image->getClientOriginalName();
        $file = $request->file('image');
        $file->move('img/articles', $name);
        $request->image = $name;
        $article->title_en = $request->title_en;
        $article->title_pl = $request->title_pl;
        $article->title_ru = $request->title_ru;
        $article->slug = $request->slug;
        $article->description_pl = $request->description_pl;
        $article->description_en = $request->description_en;
        $article->description_ru = $request->description_ru;
        $article->short_description_pl = $request->short_description_pl;
        $article->short_description_en = $request->short_description_en;
        $article->short_description_ru = $request->short_description_ru;
        $article->image = $name;
        $article->author = $request->author;
        $article->reading_time = $request->reading_time;
        $article->topic_id = $request->topic_id;
        $article->save();

        return redirect()->route('articles.index')->with('success', 'Article was created successful');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $topics = Topic::all();
        $article = Article::where('id', $id)->first();

        return view('admin.articles.edit')->with('article', $article)->with('topics', $topics);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
        $article = Article::where('id', $id)->first();
        $old_image = $article->image;
        if (isset($request->image)){
            $name = Carbon::now()->toTimeString(). '_' . Carbon::now()->toDateString(). '_' .$request->image->getClientOriginalName();
            $file = $request->file('image');
            $file->move('img/articles', $name);
            $article->title_pl = $request->title_pl;
            $article->title_en = $request->title_en;
            $article->title_ru = $request->title_ru;
            $article->slug = $request->slug;
            $article->description_pl = $request->description_pl;
            $article->description_en = $request->description_en;
            $article->description_ru = $request->description_ru;
            $article->short_description_en = $request->short_description_en;
            $article->short_description_pl = $request->short_description_pl;
            $article->short_description_ru = $request->short_description_ru;
            $article->author = $request->author;
            $article->reading_time = $request->reading_time;
            $article->topic_id = $request->tag_id;
            $article->image = $name;
            $article->save();
            unlink(public_path('/img/articles/'.$old_image));
        } else {
            $article->update($request->all());
        }
        return redirect()->route('articles.index')->with('success', 'articles was successful updated');
        } catch (\Exception $e){
            return redirect()->back()->with('error', (string)$e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
        $article = Article::find($id);

        unlink(public_path('/img/articles/'.$article->image));
        $article->delete();

        return back()->with('success', 'Article was successful deleted');
        } catch ( \Exception $e){
            return back()->with('error', (string)$e->getMessage());
        }
    }
}
