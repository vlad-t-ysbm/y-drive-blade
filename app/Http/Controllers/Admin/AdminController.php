<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function redirect(){
        return redirect()->to('/');
    }

    public function admin(){
        if (Auth::check()){
            return redirect()->to('/admin/articles');
        } else {
            return view('auth.login');
        }
    }
}
