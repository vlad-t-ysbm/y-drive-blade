<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PageRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;
use App\Page;
use Illuminate\Support\Str;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Page::orderBy('created_at', 'desc')->get();

        return view('admin.pages.index')->with('pages', $pages);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pages = Page::where('parent_id', 0)->get(['id', 'title']);

        return view('admin.pages.create')->with('pages', $pages);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Page $page, PageRequest $request)
    {
        try{

        $page->create($request->all());

        return redirect()->route('pages.index')->with('success','The page '.$request->title. ' was successfully created.');
        } catch (\Exception $e){
        return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = Page::find($id);
        $pages = Page::where('parent_id', 0)->get(['id', 'title']);
        return view('admin.pages.edit')->with('page', $page)->with('pages', $pages);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {   $page = Page::where('id', $id)->first();

        try{
            $request->validate([
                'slug' => 'unique:pages,id',
            ]);
            if (!isset($request['main_page'])){
                $request['main_page'] = 0;
            }
        $page->update($request->all());

        return redirect()->route('pages.index')
        ->with('success',$page->title.' page updated successfully');
        } catch (\Exception $e){
            return back()->with('error', 'The slug has already been taken.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page = Page::find($id);

        if($page->children()->count()){
            return back()->with('error', 'The page '.$page->title.' is the parent of the pages. You cannot delete a parent page that has sub-pages.');
        } else {
        $page->delete();

        return redirect()->route('pages.index')
            ->with('success',$page->title.' page deleted successfully');
        }
    }

    public function setMainPage($id){
        try{
        $main = Page::where('id', $id)->first();
        $main_page = Page::where('id', $id)->update(
            ['main_page' => 1]
        );
        $page = Page::where('id', '!=', $id)->update(
            ['main_page' => 0]
        );
            return redirect()->back()->with('success', $main->title.' now the main page');
        } catch (\Exception $e){
            return redirect()->back()->with('error', (string)$e->getMessage());
        }

    }
}
