<?php

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Controller;
use App\Http\Requests\MailRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;

class SendMailController extends Controller
{
    public function submit(Request $request) {
        $lang = $request->lang;

        App::setLocale($lang);
        Lang::setLocale($lang);

        $this->validate($request, [
            'name' => 'required|alpha ',
            'phone' => 'required|integer'
        ]);

        try {
            $params = [
                'name' => $request->name,
                'phone' => $request->phone,
            ];
            Mail::send('mail', $params, function ($message) use ($request) {
                $message->to('vlad.t.ysbm@gmail.com')->subject('Request from your site'
                );
            });

            return back()->with('success',Lang::get('landing.success-submit'));
        }
        catch (\Exception $e){
            return back()->with('error', Lang::get('landing.error-submit'));
        }
    }

    public function submitRent(Request $request) {
        $lang = $request->lang;

        App::setLocale($lang);
        Lang::setLocale($lang);

        $this->validate($request, [
            'name' => 'required|alpha ',
            'phone' => 'required|integer'
        ]);
        $category = collect(config('categories'))->where('id', $request->category)->first();

        try {
            $params = [
                'name' => $request->name,
                'phone' => $request->phone,
                'category' => $category['title_pl'],
                'rent' => $request->title,
                'price' => $request->price,
            ];
            Mail::send('mail', $params, function ($message) use ($request) {
                $message->to('vlad.t.ysbm@gmail.com')->subject('Request from your site'
                );
            });

            return back()->with('success',Lang::get('landing.success-submit'));
        }
        catch (\Exception $e){
            return back()->with('error', Lang::get('landing.error-submit'));
        }
    }
}
