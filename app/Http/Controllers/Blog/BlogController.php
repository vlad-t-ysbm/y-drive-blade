<?php

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Controller;
use App\Page;
use App\Topic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param $lang
     * @return \Illuminate\Http\Response
     */
    public function index($lang)
    {
        App::setLocale($lang);
//        $pages = Page::where('parent_id', 0)->orderBy('created_at', 'desc')->get();
//        $main = Page::where('main_page', 1)->first();
//        if(!isset($main)){
//            $main = Page::where('parent_id', 0)->orderBy('created_at', 'desc')->first();
//        }
        $contacts = collect(config('contacts'));
        $categories = collect(config('categories'))->keyBy('id');
        $advatages = collect(config('advantages'));
        $conditions = collect(config('conditions'));
        $reviews = collect(config('reviews'));
        $conditions_arr = [];
        $advatages_arr = [];
        $reviews_arr =[];
        foreach ($categories as $key => $category) {
            $advatages_arr[] = $advatages->where('category_id', $category['id'])->values();
            $conditions_arr[] = $conditions->where('category_id', $category['id'])->values();
            $reviews_arr[] = $reviews->where('category_id', $category['id'])->values();
        }
        $advatages = $advatages_arr;
        $conditions = $conditions_arr;
        $reviews = $reviews_arr;
        return view('index')->with('categories', $categories)->with('advantages', $advatages)->with('conditions', $conditions)->with('lang', $lang)->with('contacts', $contacts)->with('reviews', $reviews);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $page = Page::where('slug', $slug)->first();
        $pages = Page::where('parent_id', 0)->orderBy('created_at', 'desc')->get();

        return view('inner')->with('page', $page)->with('pages', $pages);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function changeUrl($url, $lang){
        dd($url);
    }
}
