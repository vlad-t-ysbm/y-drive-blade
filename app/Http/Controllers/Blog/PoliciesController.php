<?php

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Controller;
use App\Page;
use Illuminate\Http\Request;

class PoliciesController extends Controller
{
    public function coockiePolicy($lang){
        $contacts = collect(config('contacts'));
        $pages = Page::where('parent_id', 0)->orderBy('created_at', 'desc')->get();
        return view('policies.coockiePolicy')->with('pages', $pages)->with('lang', $lang)->with('contacts', $contacts);

    }

    public function legalData($lang){
        $contacts = collect(config('contacts'));
        $pages = Page::where('parent_id', 0)->orderBy('created_at', 'desc')->get();
        return view('policies.legalData')->with('pages', $pages)->with('lang', $lang)->with('contacts', $contacts);

    }
}
