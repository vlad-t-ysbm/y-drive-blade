<?php

namespace App\Http\Controllers\Articles;

use App\Article;
use App\Http\Controllers\Controller;
use App\Page;
use App\Topic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use V8Js;

class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($lang)
    {
        $contacts = collect(config('contacts'));
        $data = Article::orderBy('created_at', 'desc')->limit(3)->get();
        $topics = Topic::all();

        return view('blog')->with('data', $data)->with('topics', $topics)->with('lang', $lang)->with('contacts', $contacts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($lang, $slug)
    {
        $contacts = collect(config('contacts'));
        $article = Article::where('slug', $slug)->first();
        $topics = Topic::all();
        return view('single-article')->with('topics', $topics)->with('article', $article)->with('lang', $lang)->with('contacts', $contacts);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function showBySlug(){
        $pages = Page::where('parent_id', 0)->orderBy('created_at', 'desc')->get();
//        $topic_id = Topic::where('slug', $slug)->first();
//        $articles = Article::where('topic_id', $topic_id->id)->get();
        $topics = Topic::all();
        return view('topics')->with('pages', $pages)->with('topics', $topics);
    }

    public function load_data($lang, Request $request){
        $limit = 3;
        $page = $request->page ? $request->page : 0;
        $topic = $request->topic ? $request->topic : null;
        $search = $request->search ? $request->search : '';
        if($request->ajax())
        {
            $query = Article::orderBy('created_at', 'DESC');

            if ($topic)
                $query = $query->where('topic_id', $topic);

            if ($search){

                $query = $query->where(function ($query) use ($search, $lang) {
                    return $query->where('title_'.$lang, 'LIKE', '%'.$search.'%')
                        ->orWhere('short_description_'.$lang, 'LIKE', '%'.$search.'%')
                        ->orWhere('description_'.$lang, 'LIKE', '%'.$search.'%');
                });
            }
            $count = $query->count();
            $pages = ceil($count / $limit);
            $offset = $limit * $page;

            $data = $query
                ->offset($offset)
                ->limit($limit)
                ->get();
            $output = '';
            if(!empty($data))
            {
//                foreach($data as $key => $value)
//                {
//                    $output .= view('components.article')->with('value', $value)->render();
//                    $last_id = $value->id;
//                }
                $output .= view('components.article')->with('data', $data)->with('lang', $lang)->render();
            }

            return response()->json([
                'output' => $output,
                'pages' => $pages,
                'page' => ++$page
            ]);
        }
    }

}
